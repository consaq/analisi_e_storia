% sezione2.tex

\section{La Musica Spettrale: Definizione e Prospettive}

\subsection{Approccio Generale}
Prima di entrare in ulteriori dettagli, è necessario interrogarsi sulla definizione di musica spettrale. Come spiega Joshua Fineberg \cite{doi:10.1080/07494460000640221}, la musica spettrale non può essere definita in base agli espedienti tecnici utilizzati, bensì al modo in cui questi compositori si approcciano al suono. È bene puntualizzare che Dufourt, Grisey e Murail hanno delle visioni differenti in merito.

\subsection{Sfumature di pensiero: Dufourt, Grisey e Murail}
Murail si discosta dagli appellativi, a sua detta, impropri, circa i termini "Musica spettrale", compositori "Spettralisti" e "spettralismo". Mentre Dufourt nei suoi scritti parla di "movimento spettrale", per Murail è il fare compositivo ad avere intenzioni spettrali, osservando il suono nel suo continuo divenire. Fineberg, infatti, osserva che Murail considera la composizione spettrale non come un insieme di tecniche definite, ma come un atteggiamento nei confronti della musica e della composizione:

\citazioneestesa{"The term spectral music was coined by Hugues Dufourt. However, the most pertinent remark for understanding its meaning was made by Tristan Murail when he referred to spectral composition as an attitude towards music and composition, rather than a set of techniques. This attitude takes on broad aesthetic consequences instead of specific stylistic ones. Thus spectral composers may have vastly different styles and some might even prefer to reject the label. However, what these composers share is a central belief that music is ultimately sound evolving in time"}{Fineberg, 2000}{2}

Questo approccio, secondo Fineberg, distingue la musica spettrale non solo dai movimenti strutturalisti post-seriali, ma anche dalle estetiche ibride neo-romantiche o post-moderne. I compositori spettrali, pur appartenendo a tre diverse generazioni e scrivendo per ogni tipo di ensemble strumentale, condividono una convinzione comune: la musica è essenzialmente suono che evolve nel tempo. Questo orientamento porta tali compositori a usare la conoscenza disponibile nei campi dell'acustica e della psico-acustica per scolpire il suono in modo più preciso, come afferma Fineberg:

\citazioneestesa{"Viewing music in this way, as a special case of the general phenomenon of sound, facilitates these composers' use of the available knowledge in the fields of acoustics and psychoacoustics within their music. They can refine their understanding of what sound is, how it may be controlled and what, ultimately, a listener will be able to perceive"}{Fineberg, 2000}{2}

Per una visione più lucida dei pensieri dei compositori che hanno vissuto questo movimento riporto alcuni scritti. Grisey in un paper del "Contemporary Music Review" \cite{doi:10.1080/07494460000640311} scrive:

\citazioneestesa{"For me, spectral music has a temporal origin. It was necessary at a particular moment in our history to give form to the exploration of an extremely dilated time and to allow the finest degree of control for the transition from one sound to the next. The series had disassociated the parameters, turning upside down the concepts of verticality and horizontality, of harmony and melody. [...] What is radically different in spectral music is the attitude of the composer faced with the cluster of forces that make up sounds and faced with the time needed for their emergence. [...] Strengthened by an ecology of sounds, spectral music no longer integrates time as an external element imposed upon a sonic material considered as being 'outside-time,' but instead treats it as a constituent element of sound itself. "}{Grisey, Fineberg, 2000}{1}

Nelle parole di Grisey si percepisce una consapevolezza derivante dallo studio della psico-acustica e una pratica di ricerca ecologica nel suono dove l'articolazione nel tempo di questo lo porta a desumere gli aspetti formali della composizione. Scrive ancora:

\citazioneestesa{"The spectral adventure has allowed the renovation, without imitation of the foundations of occidental music, because it is not a closed technique but an attitude. Also, all ideas of rupture with musical tradition seem illusory to me. Le Corbusier said, architecture magnifies Space. Today, as in the past, music transfigures Time."}{Grisey, Fineberg, 2000}{3}

L'importanza del tempo come qualità intrinseca del suono si manifesta nel suo ciclo \textit{Les Espaces Acoustiques} in cui emerge la dimensione ipnotica di elementi mobili in un continuo divenire. 
Grisey suggerisce l'aggettivo "liminale" per descrivere questa tendenza, evidenziando l'ossessione di superare i confini tra i parametri musicali. L'idea di "liminale" implica che la musica non è più definita in termini rigidi, ma piuttosto come un campo fluido in cui i parametri possono sovrapporsi e interagire in modi complessi. Ad esempio, i suoni complessi che Grisey e altri compositori spettrali esplorano possono includere spettri armonici e non armonici,
suoni di campane oppure certi suoni ottenuti dalla sintesi FM, dove sfuma la distinzione tra timbro e armonia.

Angelo Orcalli \cite{orcalli_bellezza_invisibile} osserva che Grisey, pur consapevole della sintesi numerica del suono, si oppone alla visione informatica della comunicazione, criticando la ‘fedeltà’ di trasmissione delle informazioni, che presenta limiti di approssimazione. Nella sua scrittura orchestrale, enfatizza ogni battimento e suono, esaltando l'irrequietezza della materia sonora. Per Grisey, il tempo rappresenta una qualità. Tuttavia, la sua scelta dello spettro armonico del Mi grave per le sue opere implica un compromesso con il principio di indeterminazione. Grisey notò la coincidenza storica tra la nascita del pensiero spettrale e la teoria frattale, entrambi focalizzati sugli aspetti qualitativi dei fenomeni. Nonostante l’idiosincrasia di Grisey verso la riduzione della musica a modelli scientifici astratti, egli era affascinato dalla nuova visibilità scientifica. Dopo gli anni Sessanta e Settanta, vi fu un rinnovato interesse per gli strumenti acustici, ma le tecniche elettroacustiche influenzarono la ricerca di dinamiche strumentali che esaltassero risonanze insolite. Grisey sperimentò con i suoni multifonici del clarinetto, esaltandone le componenti attraverso altri strumenti, come il trombone. Nella sua opera, l'instabilità e l'emergenza spontanea dell'ordine in punti critici diventano singolarità inarmoniche e gesti musicali inusuali.

Murail sottolinea come gli strumenti musicali, nella loro forma attuale, siano il risultato di secoli di evoluzione e affinamento, raggiungendo un livello che li rende particolarmente adeguati alle nostre aspettative culturali. Questo processo di perfezionamento ha portato i compositori, come Murail, a considerarne le sonorità come un modello per la costruzione della forma musicale.
Egli sostiene che i suoni strumentali, così strettamente connessi alla nostra cultura
musicale, possano essere utilizzati non solo come elementi espressivi, ma come veri e
propri strumenti per l'organizzazione della struttura musicale. Questo approccio,
sperimentato negli anni '70, ha trovato realizzazione concreta in opere come il ciclo
\textit{Les Espaces acoustiques} di Grisey dove il compositore utilizza la risonanza naturale degli strumenti per
creare una continuità sonora che trascende l'armonia tradizionale. Secondo Murail,
questo tipo di approccio dimostra come timbro e risonanza possano essere
impiegati non solo per arricchire il colore musicale, ma per plasmare la forma stessa, creando un legame diretto tra la fisica del suono e la costruzione
formale dell'opera.

Dall'intervista di Smith a Murail:

\citazioneestesa{"I think that it is chiefly an attitude toward musical and sonic phenomena, although
it also entails a few techniques, of course. We were trying to find a way out of the
structuralist contradiction. At the same time, we did not want to be completely
intuitive like the aleatoric composers or like Giacinto Scelsi, or even Gyorgy Ligeti
(who was and remains an experimentalist but an intuitive composer). We were interested
in those experi- ences, but we wanted to build something more sound (pun intended).
This was part of it, as was a curiosity about sounds. "}{Murail, Smith, 2000}{12}

Sottolineamo, in questo passaggio, il richiamo ad alcuni altri compositori,
in particolare con Giacinto Scelsi (1905-1988).

\subsection{L'influenza di Giacinto Scelsi}
Scelsi, compositore italiano romano attivo dalla metà del Novecento, è spesso citato come uno dei precursori di questa tendenza, pur non essendo formalmente parte del movimento spettrale. Le sue innovazioni nella gestione del suono, l’uso dei microintervalli e la ricerca di una dimensione ultraterrena attraverso la musica hanno profondamente influenzato molti compositori spettrali, tra cui Murail.

Uno degli aspetti fondamentali dell'influenza di Scelsi è la sua radicale esplorazione del suono come oggetto centrale della composizione. A partire dagli anni 1950, Scelsi iniziò a concentrarsi su singole note o su semplici suoni, abbandonando progressivamente le strutture musicali tradizionali basate su melodie e armonie. Questo approccio si riflette in opere come \textit{Quattro pezzi per orchestra (ciascuno su una nota)} (1959), dove ogni pezzo è costruito attorno a una singola nota, come punto di partenza per variazioni interne, attraverso tecniche come il vibrato e i microintervalli. Come osservato nel testo di Anderson \cite{Anderson2000APH}, Scelsi riusciva a creare una "profondità del suono", un concetto che consisteva nel trasformare la stessa nota in un oggetto di esplorazione infinita grazie all’uso di trilli, tremoli e variazioni dinamiche​ \cite{Anderson2000APH}.

Questa attenzione alla dimensione timbrica e alle microvariazioni all'interno di un singolo suono ha avuto una forte risonanza nella musica spettrale. Murail si è spesso ispirato a Scelsi. Sebbene l’approccio tecnico di Murail fosse più scientifico rispetto a quello intuitivo di Scelsi, i due compositori condividono un interesse per la decostruzione del suono. Murail stesso, in varie interviste e scritti, ha sottolineato come l’attenzione di Scelsi per il suono puro, la sua decomposizione attraverso tecniche microtonali e la sua esplorazione del timbro siano stati fondamentali per la sua musica. In particolare, in \textit{Désintégrations} (1982-83), Murail utilizza tecniche appartenenti ai processamenti elettronici per elaborare la scrittura strumentale. Questo processo richiama l'approccio di Scelsi nel "decomporre" il suono in componenti più piccoli e nel manipolarli per creare texture complesse​ \cite{doi:10.1080/07494460500154889}.

L'uso dei quarti di tono è un'altra area di connessione tra Scelsi e la musica spettrale. Scelsi impiegava sistematicamente i microintervalli per creare effetti sonori inediti, come l'addensamento e la dilatazione dell'unisono, che conferivano alle sue composizioni una ricchezza timbrica unica. Murail e altri compositori spettrali hanno portato avanti questa ricerca, utilizzando i microintervalli per generare battimenti e variazioni timbriche complesse. Nel caso di Murail, i quarti di tono diventano fondamentali per un mondo sonoro in continua evoluzione, dove le sfumature tonali creano una sorta di tensione percettiva che si trasforma lentamente nel tempo​.

Un altro parallelo significativo tra Scelsi e la musica spettrale riguarda la trasformazione graduale del suono. In molte opere di Scelsi, come il  Quartetto d'archi No. 4 (1964), la musica si sviluppa attraverso l'evoluzione lenta e continua di una nota o di un insieme di note, con l’aggiunta progressiva di suoni circostanti che si fondono e si trasformano. Questo processo di evoluzione graduale è centrale anche nella musica spettrale, dove la transizione continua tra differenti spettri sonori diventa una delle caratteristiche distintive della forma musicale. Murail, ad esempio, nel suo \textit{Désintégrations} elimina il parametro dell’altezza per concentrarsi sull’evoluzione dello spettro, una scelta che richiama le esplorazioni di Scelsi nel mondo delle microvariazioni sonore​\cite{doi:10.1080/07494460500154889}.


Infine, un aspetto più concettuale che accomuna Scelsi e i compositori spettrali è la loro relazione con il tempo musicale. La musica di Scelsi non segue lo sviluppo tematico tradizionale, ma piuttosto un flusso continuo e meditativo che sembra abolire il concetto di tempo lineare. Murail e altri compositori spettrali adottano una concezione del tempo simile, dove la struttura musicale si sviluppa senza eventi drammatici o cambiamenti improvvisi, ma piuttosto attraverso processi sonori lenti e costanti, simili a un'evoluzione naturale. Questo legame con il tempo e l'esplorazione dell'infinitamente piccolo nel suono ricollegano il pensiero di Scelsi a una visione più ampia della musica contemporanea, nella quale il suono stesso diventa il centro della creazione artistica \cite{doi:10.1080/07494460500154889}.



\subsection{L'Interesse per gli Idiofoni}
Questo interesse per lo spettro in costante mutazione spinge i compositori spettralisti a una particolare attenzione degli idiofoni. Secondo la definizione di Erich Moritz von Hornbostel e  Curt Sachs, sono strumenti il cui suono è prodotto dalla vibrazione del corpo stesso dello strumento, senza l'utilizzo di corde o membrane tese e senza che sia una colonna d'aria a essere fatta vibrare \cite{hornbostel_sachs_1914}. L'interesse per gli strumenti a percussione segue un filo rosso: Murail è allievo di Messiaen che fondò "La jeune France" \cite{brockington2008creative} insieme ad Jolivet e come osservato precedentemente Messiaen ci tiene a sottolineare l'influenza del collega sulla sua poetica. Jolivet fu un allievo di Varese, l'apripista di un rinnovato interesse per questa famiglia strumentale e in generale per un fare compositivo progressista rivolto verso la commistione tra arte e scienza \cite{dufourt1987musica}.
Anche Dufourt scrive riguardo gli idiofoni:
\citazioneestesa{"Senza dubbio lo sconvolgimento più radicale che abbia conosciuto la musica del XX
secolo è avvenuto a livello tecnologico. È dovuto al brusco e generalizzato
avvento delle piastre e delle membrane, che progressivamente soppiantano l'antica
liuteria degli archi e dei fiati. [...] L'apporto della nuova organologia è una
poetica dell'energia sonora. [...] transitorio d'attacco e d'estinzione, profili
dinamici in costante evoluzione, rumori, suoni di massa complessa, suoni multifonici,
grana,risonanze ecc... Tutti questi procedimenti incerti e fluttuanti un tempo erano
respinti o almeno mantenuti allo stato residuale, perché uscivano dalle regole,
andavano verso il disordine. Oggi si situano al centro della creazione musicale. La
sensibilità uditiva è per così dire « rigirata» . Essa si preoccupa delle minime
oscillazioni, delle scabrosità, del tessuto sonoro. La plasticità del suono, la sua
fugacità, le sue minime alterazioni hanno acquistato una forza di suggestione
immediata."}{Dufourt, 1987}{66}


\subsubsection{Spettri armonici - inarmonici}
Il rapporto tra spettri armonici e inarmonici è una tematica pregnante delle
composizioni spettrali, in particolare di Murail. Egli descrive il fascino che i suoni
delle campane hanno esercitato sui compositori nel corso dei secoli \cite{doi:10.1080/07494460500154889}. Una campana
europea presenta una caratteristica distintiva: una terza minore all'interno di uno
spettro che altrimenti è relativamente armonico. Questa complessità arricchisce il
suono senza renderlo troppo confuso. Le campane tubolari usate in orchestra, tuttavia,
producono un suono diverso dalle campane tradizionali, con armonici che evocano una
terza minore. Anche se un compositore scrive una determinata nota per una campana
tubolare, l'ascoltatore potrebbe percepire altri suoni, come armonici più alti o
frequenze leggermente spostate. 

Murail discute poi altri strumenti a percussione
metallici, come le piccole campane giapponesi, le cui componenti sonore mostrano una
certa distorsione del normale spettro armonico. Conclude affermando che il compositore
può trattare questi suoni complessi non solo come effetti timbrici, ma integrarli nel
discorso musicale, tenendo conto delle relazioni armoniche del loro spettro.
Egli critica la mancanza di controllo sulle sonorità percussive e si propone un
approccio disciplinato per manipolare questi "oggetti sonori", riconoscendo che ogni
suono, anche se di breve durata, è un processo. Sostiene di utilizzare i suoni
strumentali come modelli formali da cui possono essere estrapolati nuovi arrangiamenti
timbrici e su cui costruire nuovi elementi formali, o addirittura l'intera
architettura di un pezzo musicale.

Nel brano \textit{Gondwana} (1980) egli esplora i suoni inarmonici, simili a quelli presenti in
\textit{Mortuos plango vivos voco} (1980), ma in questo caso le campane
utilizzate sono immaginarie. Murail utilizza una tecnica matematica chiamata
modulazione di frequenza per creare sonorità campaniformi nell'orchestra, ispirata
alla tecnica sviluppata da John Chowning (1934).
La modulazione di frequenza si basa sull'interazione tra due generatori di suono: il
"portante" e il "modulatore", che combinano le frequenze per produrre suoni
risultanti. Murail traduce le frequenze calcolate in altezze musicali, approssimandole
alle note musicali più vicine, anche usando quarti di tono.

\textit{Gondwana} inizia con intervalli dissonanti (G\# e G), per poi evolvere verso
armonie più consonanti, culminando in uno spettro armonico doppio incompleto.
L'orchestrazione è cruciale: Murail sceglie strumenti con spettri più puri, come gli
ottoni, per non complicare troppo le aggregazioni sonore. La dinamica e l'evoluzione
dei suoni ricordano il profilo di un suono di campana, con attacchi percussivi e un
progressivo estinguersi delle componenti alte.
La tecnica di modulazione di frequenza viene usata anche in altre sezioni del brano,
come nella sezione F, dove le frequenze risultanti producono strutture armonico-melodiche che si espandono come onde, partendo da intervalli ristretti per poi aprirsi
e riempire tutta la tessitura orchestrale.
In sintesi, \textit{Gondwana} fonde suoni complessi, tecniche matematiche e orchestrazione
sofisticata per creare un mondo sonoro in continua trasformazione.

L’utilizzo dell’orchestrazione spettrale si fonde con la algoritmi per generare comportamenti e con tecniche di sintesi che rendono coerente il materiale musicale complessivo.
Murail offre esempi di come le tecniche elettroacustiche influenzino le tecniche compositive, creando stati ibridi e integrando rumori e suoni complessi. Egli enfatizza il ruolo dell’intuizione nel processo compositivo. Secondo Murail, l’intuizione permette di determinare i parametri principali, come la durata globale e il comportamento desiderato, mentre l’algoritmo aiuta a definire la curva ideale e le fasi intermedie. Tuttavia, egli considera l’interazione tra l’intuizione e l’algoritmo essenziale per esplorare una gamma più ampia di possibilità creative. 

Questo concetto riflette uno dei principi chiave della composizione di Murail: il bilanciamento tra prevedibilità e imprevedibilità. Un discorso musicale totalmente prevedibile rischia di diventare noioso, mentre uno completamente imprevedibile perde d’interesse; la chiave, dunque, è una tensione tra continuità strutturale e sorpresa.
L’utilizzo della modulazione FM e altre tecniche di analisi spettro-morfologica saranno fondamentali per la scrittura del brano \textit{Désintégrations}.
Di seguito un approfondimento su questa composizione.

