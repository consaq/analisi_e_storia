\section{Analisi Desintegration}
\textit{Désintégrations} è un brano per orchestra (organico composto di:
flauto 1 (+ ottavino)
flauto 2 (+ flauto in Sol)
oboe (+ corno inglese)
clarinetto 1 in Si bemolle
clarinetto 2 in Si bemolle (+ clarinetto basso)
fagotto (+ controfagotto)
corno
tromba in Do
trombone (tenore-basso)
2 percussioni
pianoforte
2 violini
viola
violoncello
contrabbasso e tape (suoni sintetizzati mediante computer)). 

\begin{table}[htbp]
    \centering
    \begin{tabularx}{\textwidth}{|l|X|}
    \hline
    \textbf{Autore}                      & Tristan Murail (1947) \\ \hline
    \textbf{Anno o anni di lavorazione}  & 1982 \\ \hline
    \textbf{Prima esecuzione}            & 15/02/1983, Paris, Espace de projection de l'Ircam, Ensemble intercontemporain, Peter Eötvös (conductor) \\ \hline
    \textbf{Durata}                      & 22' \\ \hline
    \textbf{Registrazione utilizzata per l'analisi} & 1 CD Accord, AC4653052, \textit{Serendib - L'Esprit des dunes - Désintégrations}, Ensemble intercontemporain, David Robertson (conductor) \\ \hline
    \textbf{Edizione partitura}          & Éditions Henry Lemoine \\ \hline
    \textbf{Pubblicazione}               & 01/12/1982 \\ \hline
    \textbf{Commissionato da}            & Madame David Weil pour l’Ircam-Centre Pompidou \\ \hline
    \end{tabularx}
    \caption{Informazioni sull'opera analizzata}
    \end{table}    

E' stato commissionato dall'IRCAM e presentato per la prima volta il
15 febbraio 1983 dall'Ensemble InterContemporain, diretto da Peter Eötvös (1944–2024).
\textit{Désintégrations} è un'opera composta dopo un intenso lavoro di
esplorazione sul concetto di "spettro". Tutto il materiale del brano, sia per la parte
orchestrale che per il nastro magnetico, si basa su analisi, decomposizioni o
ricostruzioni artificiali di spettri armonici o inarmonici. Gli spettri utilizzati
provengono soprattutto da suoni strumentali, come il registro grave del pianoforte, i fiati e il
violoncello. 
La fusione tra strumenti acustici e suoni sintetici su nastro è il fulcro della
composizione, nell'intento di far comunicare questi due mondi sonori,
utilizzando per entrambi gli stessi procedimenti di generazione di armonie strumentali
e timbri sintetici, creando così una continuità tra gli elementi acustici ed
elettronici.
I contributi strumentali servono come modello per la costruzione di timbri, armonie e forme
musicali.
Murail impiega vari tipi di trattamento degli spettri, tra cui:
\begin{itemize}
\item frazionamento: si utilizza solo una parte dello spettro, come nei suoni di
campana ottenuti frazionando suoni di pianoforte.

\item Filtraggio: amplificazione o riduzione di componenti specifiche dello spettro.

\item Esplorazione spettrale: movimenti all'interno del suono, dove i costituenti
dello spettro vengono uditi in sequenza, trasformando il timbro in una melodia.

\item Creazione di spettri inarmonici: ottenuti aggiungendo o sottraendo frequenze o
torcendo lo spettro di base.
\end{itemize}
    


Il nastro è stato calcolato tramite sintesi additiva, che permette di descrivere ogni
componente del suono in modo preciso, consentendo di manipolare i parametri
con l'accuratezza desiderata. La realizzazione tecnica è stata supportata dall'uso
della nuova macchina 4X dell'IRCAM, che permetteva la sintesi in tempo reale, e da
un programma chiamato "Syntad", sviluppato dallo stesso Murail per calcolare i
parametri necessari come racconta nell'intervista citata precedentemente:

\citazioneestesa{"In fact, I simply used the stock analysis that had been done by David Wessel. They
were just text files, and I used them as the basis for the synthesis as well as for
the harmonic structures of the piece. In order to keep the richness of the
information, I decided to use additive synthesis, which is, of course, the inverse
of a Fourier transform"}{Murail, Smith, 2000}{2}

L'orchestrazione segue gli stessi principi compositivi del nastro , e spesso questo
amplifica o esagera il carattere degli strumenti orchestrali, portando la sonorità
verso una disintegrazione o una diffrazione dei timbri. Il brano è strutturato in
undici "momenti" o percorsi, collegati da transizioni o trasformazioni, con un
continuo movimento tra armonico e inarmonico, creando contrasti di luce e ombra,
ordine e disordine ritmico.

\subsection{Indicazioni in partitura}
I suoni sul supporto magnetico sono rappresentati nella partitura con simboli musicali o
grafici, e le sigle come "htb." o "tbn." indicano una qualche somiglianza con relativi
timbri strumentali. Nella partitura, ogni tempo è segnato con un tratto leggero, mentre le
misure sono separate da tratti più marcati. Le suddivisioni ritmiche delle misure sono 
per quarti o per frazioni di quarti (per esempio 1/2 e 2/3) che indicano suddivisioni di ottavi o terzine.

Per quanto riguarda la diffusione del suono, il nastro è composto di quattro tracce. Le prime due tracce devono essere
diffuse in stereofonia dai diffusori frontali, mentre la terza è destinata a
diffusori situati sul fondo della sala. La quarta traccia, che contiene i clics per la
sincronizzazione, deve essere ascoltata dal direttore tramite auricolare. Il volume
del nastro deve essere regolato per mantenere l'equilibrio con l'orchestra,
intervenendo in certi passaggi per rinforzare il nastro, come nelle sezioni II e VI.
La terza traccia viene utilizzata solo in alcuni momenti della composizione e può
essere amplificata per migliorare l'effetto di spazializzazione del suono \cite{murail1990desintegrations}.

\subsection{Sezione I}
Murail esamina un suono di pianoforte, analizzando i suoi primi cinquanta
armonici. Egli individua i formanti, ossia gruppi di parziali caratterizzati da
intensità maggiori rispetto a quelli circostanti, e li utilizza per costruire spettri
armonici, poi tradotti in notazione musicale. Da questi spettri seleziona aggregati di
suoni, che non considera come semplici accordi ma piuttosto come combinazioni
timbriche capaci di fondersi sia nel contesto strumentale che elettronico. Questi
aggregati risultano percepibili non come accordi tradizionali, ma come timbri unici e
complessi, poiché la sintesi elettronica rafforza l'unità percettiva dei suoni
sinusoidali, mentre l'orchestrazione strumentale, più ricca di armonici e dettagli
sonori, conserva un senso di armonia tradizionale.
La trama evolve timbricamente da armonica a inarmonica. Inizialmente, gli aggregati si
basano su frammenti di serie armoniche, ma con l'aggiunta di parziali inferiori e
sovrapposizioni spettrali, il suono diventa più instabile e inarmonico. Questa
trasformazione culmina nella collisione di due fondamentali, arricchite da parziali
trasposti, creando un effetto simile a un tam-tam. L'orchestrazione, che inizia con
timbri leggeri come flauti e clarinetti, diventa progressivamente più complessa,
culminando in una densa tessitura orchestrale e in un cluster finale di alta intensità
sonora.

\subsection{Sezione II} 
Si basa su modulazioni ad anello applicate a suoni complessi, creando una
frammentazione dello spettro che si manifesta in trilli asincroni e sovrapposti, un
effetto simile a quello della sezione IX.

\subsection{Sezione III}
 Utilizza la tecnica dello shifting di frequenza per trasformare un
aggregato di cinque note derivato dalla serie armonica di Fa1. Lo spostamento di
frequenza, simile alla modulazione ad anello, altera i suoni acustici aggiungendo o
sottraendo una quantità fissa di hertz, creando intervalli non lineari e inarmonici.
In questa sezione, un aggregato di suoni eseguito dai legni e dagli ottoni con sordina
si sposta gradualmente verso il registro grave, diventando sempre più inarmonico. Il
processo non avviene con un glissando continuo, ma con una serie di passaggi discreti,
in cui le frequenze si spostano per gradi successivi calcolati, producendo cambiamenti
graduali ma inaspettati nell'armonia.

\subsection{Sezione IV} 
Introduce lo shifting di frequenza utilizzando curve invece di linee, in
contrasto con la sezione III. 

\subsection{Sezione V}
 Qui le frequenze all'interno di uno spettro
armonico si spostano progressivamente, creando battimenti e un suono sempre più
ruvido. Questo è l'unico esempio di vero spettro armonico nel pezzo. 

\subsection{Sezione VI}
Una serie di modulazioni di frequenza caratterizza questa parte. 

\subsection{Sezione VII} 
Vede una rapida permutazione di sette spettri distorti armonicamente, con le parti strumentali
approssimate al semitono a causa del ritmo veloce.
Analizzando le tecniche come il ring modulation o lo shifting di frequenza, il
compositore si rende conto che presentano una spaziatura regolare simile a quella
degli spettri armonici, anche se con una differente qualità timbrica. Queste tecniche
gli risultano limitate, portando a un suono prevedibile e ripetitivo, come nel caso
della musica elettronica degli anni 1950-60. 

\subsection{sezioni VIII IX e X} 
Qui vengono esplorati modelli di spettri più complessi , come quelli degli strumenti a percussione
metallica o del pianoforte, che presentano parziali inarmonici. Il suono del
pianoforte è leggermente inarmonico, con i parziali che si discostano dalla loro
posizione teorica, soprattutto nei registri acuti. Questo fenomeno viene modellato con
un'equazione di "distorsione armonica": $p=f*r^d$, dove d rappresenta il grado di
distorsione. Quando d è maggiore di 1, la serie armonica è allungata; quando d è
inferiore a 1, è compressa.
La sezione X del brano \textit{Désintégrations} applica questa distorsione armonica al suono
del trombone, con la progressiva alterazione dei parziali tramite incrementi di quarti
di tono, portando a un'armonia fortemente inarmonica e a un aumento della tensione.
Nella sezione VIII, viene utilizzata una distorsione più complessa con due punti di
riferimento (il terzo e il ventunesimo armonico), i cui parziali si muovono
rispettivamente per mezzi e quarti di tono. Qui, le permutazioni locali dei parziali
interrompono la prevedibilità del processo, rendendo l'ascolto più dinamico e
interessante.

\subsection{Sezione XI}
 Lo spettro armonico viene trasposto di un'ottava alla volta, abbassando
gradualmente la fondamentale. Alla fine, lo spettro utilizza solo un parziale su
dieci, producendo un effetto timbrico che ricorda il suono di un tam-tam.