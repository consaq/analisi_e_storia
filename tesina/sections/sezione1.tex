\section{Riflessioni sul temperamento nella musica}

\subsection{Dal sistema della scala naturale al temperamento equabile}
Storicamente, il passaggio dal sistema della scala naturale al sistema del temperamento equabile avviene attorno al XIX secolo, dopo varie trattazioni e querelle tra i teorici della musica su quale fosse il sistema migliore per comodità e coerenza musicale. La motivazione principale dell’utilizzo del sistema temperato ci viene fornita in maniera pratica da Johann Sebastian Bach (1685–1750) nella sua opera \textit{Das Wohltemperirte Clavier, oder Praeludia, und Fugen durch alle Tone und Semitonia…} (1722, 1744) (nonostante recenti studi sembrino confutare questa teoria), attraverso la quale era possibile modulare da una regione tonale all’altra senza problemi di intonazione grazie a un sistema di quinte tutte ugualmente calanti. Tuttavia, questo metodo, non affidandosi alla modellazione fisica della natura, tende a creare problemi percettivi a livello formale (di forma intesa come durata).

\subsection{Implicazioni del temperamento equabile sulla percezione musicale}
In un dibattito recente tra Walter Branchi (1941) e Giorgio Nottoli (1945) all'Università di Tor Vergata, emerge una critica su ciò che viene definito il discorso musicale \cite{branchi2024incontro}.
Secondo Branchi l’utilizzo del sistema temperato è necessario per un atteggiamento musicale narrativo. I temi vengono esposti, variati e intrecciati all’interno di un ritmo armonico, con cadenze evitate che ritardano la cadenza perfetta. Il ritardase la cadenza perfetta serve a compensare i difetti del temperamento che risulta innaturale all'ascolto su durate musicali lunghe. 
Lo sviluppo dell'udito è dettato anche da agenti ambientali e culturali e intre secoli il nostro udito è stato viziato da questo sistema nonostante non sia progettato geneticamente a riconoscere terze o quinte calanti rispetto alla scala naturale degli armonici. 
Viene in aiuto Luigino Pizzaleo al quale mi appoggio per le successive spiegazioni \cite{pizzaleo_branchi_2022}.
Citando testualmente

\citazioneestesa{"Se infatti poniamo mente alle prime forme della polifonia francese – ad esempio l'organum di Nôtre Dame – rileviamo la sostanziale assenza di una direzionalità affermata, mentre il tessuto vocale sovrastante la vox organalis appare più come la manifestazione simultanea dell'intero potenziale polifonico ammesso dalle restrizioni del sistema (un sistema totalmente
espresso), e dunque come l'esposizione, senza ellissi e senza scelte, di tutte
le possibilità che il sistema stesso offre al compositore, che come lo sviluppo di una drammaturgia sonora per giustapposizione di oggetti e temi e per sillogismi. "}{Pizzaleo, Branchi, 2024}{61 \cite{branchi2024incontro}}

e ancora:

\citazioneestesa{"Dato un certo numero di talee e colores, il mottetto rende manifeste una serie di possibilità implicite al sistema descritto da tali elementi, i quali del resto, nella loro natura di materiali precomposti e
dunque preesistenti alla musica, denunciano una condotta potenzialmente
tematica [...] La polifonia spessa e potente di Ockeghem, in cui il flusso prevale sugli oggetti isolabili, rinvia ad un approccio sistemico, mentre l'individuazione frastica ormai pienamente realizzata in Josquin Desprez sembra ormai prevalentemente votata al tematismo"}{Pizzaleo, Branchi, 2024}{61}

La musica tematica, comune nella tradizione occidentale, si basa su un tema che nell'arco formale subisce variazioni. Questo approccio narrativo è radicato in unità giustapposte che trovano significato solo all'interno di una struttura complessiva e in un preciso ordine temporale. Il tematismo è finito e lineare, rappresentando una narrazione musicale che, pur astratta, segue una logica temporale e diegetica, come accade nella musica di compositori come Ludwig van Beethoven (1770–1827) o Johannes Brahms (1833–1897).
In netto contrasto, nella concezione sistemica descritta da Branchi, la musica procede per differenziazione delle componenti implicite di un sistema, piuttosto che seguire un percorso lineare da un inizio a una fine.
Tuttavia, con l’avvento dell'Umanesimo e di compositori come Josquin Desprez (1450–1521), emerge progressivamente una maggiore direzionalità tematica, che rispecchia un parallelismo con lo sviluppo del linguaggio verbale.

Scrive ancora Pizzaleo che la musica sistemica, secondo Branchi, è una forma che si manifesta nel tempo senza affermare il suo sviluppo temporale. Essa non rappresenta qualcosa di esterno, ma è "puro suono" che si modifica senza aspettative, illuminando il tempo come un reagente. 
Accantoniamo le riflessioni di Branchi, vi torneremo tra qualche paragrafo. Poniamo l'attenzione sul temperamento e sul conseguente linguaggio tonale.

\subsection{Monotonalità e modulazione: la narrazione musicale nel periodo classico e romantico}
Nel periodo classico e fino al tardo romanticismo, si utilizzava una monotonalità \cite{schonberg2014trattato}. 
Le modulazioni erano un escamotage per aprire dei percorsi, delle strade con cui esporre il discorso musicale. Una narrazione. 
La costruzione del pianoforte è stato un punto cardine nell’ispessimento dello spettro di temperamenti e accordature nella prassi strumentale. 
La sua entrata nei salotti borghesi ha contribuito alla diffusione di una pratica dilettantistica basata sulle trascrizioni per tastiera e la comodità di questo strumento polifonico
di suonare più altezze ha contribuito a una prassi compositiva in cui 
il pianoforte era lo strumento di pensiero e di partenza per la scrittura di linee melodiche di altri strumenti, soprattuto in ambito orchestrale.
Questo fenomeno ha contribuito allo spostamento dell'attenzione dei compositori verso il dominio delle altezze, temperate.

\subsection{L'accordatura fissa del pianoforte e il suo impatto sulla musica d’insieme}
Il pianoforte, con la sua accordatura fissa, fungeva da riferimento a cui tutti gli altri strumenti dovevano riferirsi in situazioni cameristiche. 
Questo porta alla centralità del discorso musicale e, più precisamente, al linguaggio tonale, che portato all’esasperazione nel tardo romanticismo conduce Schoenberg a rompere i rapporti di subordinazione nella monotonalità, verso un appiattimento delle relazioni con il centro tonale.



\subsection{Schoenberg, Hindemith e l'emancipazione della dissonanza}
Schoenberg, nell’emancipare la dissonanza, introduce il concetto di consonanze lontane, riferendosi alla scala naturale degli armonici, pur continuando a utilizzare il sistema temperato. Tuttavia, percettivamente parlando, i brani di Schoenberg risultano difficili da ascoltare su lunghe durate. Nello stesso periodo, altri compositori cercarono soluzioni al linguaggio tonale, pur rimanendo legati al sistema temperato. Uno dei più noti è Igor Stravinsky (1882–1971), che nella \textit{Sagra della Primavera}, con l’uso della poli-tonalità, esplora nuovi colori orchestrali. L'articolo di Julian Anderson è illuminante sotto questo aspetto \cite{Anderson2000APH} ; riporterò qui alcuni dei punti fondamentali.

Harry Partch (1901-1974), uno dei pionieri dell’uso dello spettro armonico nella musica, ha sviluppato una scala di 43 note all'ottava, fondata su intervalli giusti derivati dallo spettro stesso. Nel suo trattato \textit{Genesis of a Music}, Partch non solo teorizza queste idee, ma costruisce anche strumenti musicali specifici per eseguire opere scritte secondo questa scala, dimostrando un forte legame tra teoria acustica e pratica compositiva.

Successivamente, Henry Cowell  (1897-1965) nel suo lavoro \textit{New Musical Resources} ha ampliato il concetto di spettro armonico, esplorando nuovi sistemi armonici e poliritmi. Cowell si è concentrato sull’associazione tra composizione timbrica e schemi ritmici, anticipando concetti che influenzeranno la musica di compositori come Karlheinz Stockhausen (1928-2007). Ha anche proposto l'idea dei "sottotoni", in base all'inversione dello spettro armonico.

Hindemith, nel suo trattato \textit{The Craft of Musical Compositions}, ha integrato l'analisi dello spettro armonico per sviluppare scale che esplorano le relazioni tra consonanza e dissonanza. Ha posto particolare enfasi sui toni di somma e differenza come elementi fondamentali della sua musica, cercando di stabilire una connessione “naturale” tra la teoria acustica e la composizione musicale. Le sue idee hanno avuto un impatto significativo su compositori successivi, come Grisey.

Infine, Olivier Messiaen (1908–1992) ha incorporato lo spettro armonico nel suo approccio compositivo, utilizzando concetti come la "risonanza" per giustificare il suo impiego di armonie insolite, tra cui la quarta aumentata. Nelle sue opere, come \textit{Couleurs de la Cité Céleste}, Messiaen esplora timbro e risonanza, combinando elementi di armonia e timbro per creare una musica che richiama suoni naturali, come il canto degli uccelli.
Messiaen riconosce di essere stato fortemente influenzato dalla musica del suo collega André Jolivet (1904-1974), le cui prime opere per pianoforte, come \textit{Mana} (1935) e \textit{Cinq Danses Rituelles} (1939), presentano varietà affascinanti di tali risonanze. 

Jolivet, è stato allievo diretto di Edgard Varèse (1883-1965) in europa. Quest'ultimo è considerato un precursore della composizione spettrale. Varèse riteneva che lo spettro armonico fosse un fenomeno fondamentale e il suo fascino per il suono per il suono stesso ha portato a opere in cui il timbro diventa un principio compositivo. Anderson si esprime su Varèse chiamando in causa il compositore Morton Feldman (1926–1987) \cite{Anderson2000APH}:

\citazioneestesa{The American composer Morton Feldman, who knew Varèse well in the forties and fifties, has remarked upon the fact that 'the matching in Varèse's music of harmonic aggregate to instrumental timbres is almost uncanny,' and indeed a work like 'Integrales' (1924) largely consists of blocs sonores, each sharply defined in register, intervallic content and instrumentation. Analysis of such phenomena has so far proved problematic, as conventional analytic methods have developed extremely refined means of examining pitch but not 
timbre."}{Anderson, 2000}{12}
\subsection{Il serialismo e la musica dopo la Seconda Guerra Mondiale}
Nei Ferienkurse di Darmstadt, si abbandona l’interesse per il materiale musicale a scopi narrativi, avvicinandosi alla pratica seriale, soffermandosi su percorsi parametrici della nota indipendenti l’uno dall’altro. Questo fenomeno culturale interessa gran parte dei compositori europei dal 1940 al 1960, ma produce scarsi risultati riguardo alle problematiche musicali sollevate e risolte. Un'esperienza da tenere in considerazione è la realizzazione  \textit{Studie II} (1954) di Stockhausen \cite{stockhausen1956studieII} e di \textit{Incontri di fasce sonore} (1957) di Franco Evangelisti (1926-1980) \cite{evangelisti1963incontri} presso lo Studio di Musica Elettronica W.D.R. di Colonia. Queste composizioni affrontano l'organizzazione delle altezze con nuove forme di temperamento. La necessità di una nuova divisione delle altezze all'infuori del temperamento equabile è evidente. Questi compositori ricercano un nuovo sistema entro cui orientarsi. 
Confrontando le indicazioni di Stochausen in partitura:

\citazioneestesa{A frequency scale of 81 steps is chosen from 100 c.p.s. upwards with the constant interval ratio $\sqrt[25]{5}$
​. The frequencies are rounded off to the values obtainable from the RC oscillator used. Five sinusoidal notes with constant intervals are compounded for each mixture (see "Realisation"). Five different note mixtures are used, the constant intervals being 1, 2, 3, 4, or 5 times $\sqrt[25]{5}$
​ respectively. The resulting note mixtures numbered from 1 to 193 constitute the sound material for this study (in c.p.s.).}{Stockhausen, 1954}{3}

e quelle di Evangelisti:

\citazioneestesa{La suddivisione dello spazio sonoro e delle sue durate, è stata concepita in base ai rapporti psicofisici ed in funzione dei parametri degli stessi. La scala delle frequenze è stata suddivisa in 91 gradi a partire dalla frequenza più bassa di 87 Hz alla più alta di 11.950 Hz.
Il problema fondamentale è stato quello di non stabilire rapporti di armonica di nessun genere, espressi mediante numeri razionali interi, come ad esempio nella scala temperata il rapporto di 2 : 1, o come nello studio elettronico di K. (Stockhausen) il rapporto di 5 : 1.
[...] I limiti di questa variabilità sono stati determinati tenendo conto dei valori di massima e di minima della soglia di udibilità e di esatto riconoscimento dei medesimi.
La curva rappresentatrice è logaritmica. I valori di Max./1,0725 e di Min./1,04 sono risultanti delle medie statistiche di tale curva.}{Evangelisti, 1958}{4}

si osservano delle differenze sostanziali. Evangelisti è focalizzato sugli aspetti percettivi del suono, enfatizzando la suddivisione delle frequenze e delle durate in funzione dei rapporti psicofisici, cioè per come l’orecchio umano percepisce le variazioni di altezza e durata. Si parla della difficoltà di stabilire rapporti armonici riconoscibili attraverso numeri razionali, sottolineando come la percezione uditiva influenzi la scelta delle frequenze.
In Stockhausen l’approccio è più tecnico e sperimentale, con la scelta di un preciso intervallo costante ($\sqrt[25]{5}$) per dividere le frequenze in 81 step. Quest'ultimo punta meno agli aspetti percettivi e più alla realizzazione pratica di note o meglio di miscele con rapporti di frequenza costanti.

D'altra parte la pratica della serializzazione integrale di ogni parametro della nota riflette una visione astratta dei parametri della nota, che perde il contatto con la materia e la natura fisica del suono. 


\subsection{La critica della musica spettrale al serialismo}

Nel 1972, Grisey partecipa ai seminari di Darmstadt con figure come Stockhausen e Iannis Xenakis (1922–2001), in un periodo di grande fermento culturale e di cambiamento. Si assiste a un passaggio dalla composizione attraverso suoni di sintesi montati su nastro magnetico a forme più libere di elaborazione del suono. Durante gli anni Settanta, la musica si evolve in un contesto di innovazioni tecnologiche e teorie sistemiche, come l'ecologia della percezione e la teoria dei frattali, che mettono in discussione le categorie tradizionali della ricerca musicale \cite{orcalli_bellezza_invisibile}.

Con lo sviluppo degli studi di acustica e psicoacustica, si osserva che nella modellazione fisica dei suoni, i parametri sonori sono strettamente correlati e si influenzano a vicenda. La visione astratta della nota nella musica seriale è uno dei motivi che sancisce la rottura con la scuola di Darmstadt. A partire dagli anni ‘70, i compositori Gérard Grisey, Tristan Murail, Michaël Levinas (1949) e Roger Tessier (1939) fondano l’ensemble L’Itinéraire, con l’interesse di esplorare un'idea di composizione differente dal serialismo integrale scontrandosi con le politiche estetiche dei poli culturali europei di Pierre Boulez (1925-2016) in Francia e di Stockhausen in Germania. 
Citando l'intervista di Ronald Bruce Smith a Murail \cite{smith2000interview}:

\citazioneestesa{In a way, serialism was the establishment. It certainly became the establishment when Pierre Boulez came back to Paris in the mid-1970s to establish IRCAM and the Ensemble
Intercontemporain. He was "establishment," as he was heavily supported by the government. It seemed that there was little room for anything else. While there were other ensembles in Paris, such as L'Itineraire, we had to fight just to survive, to keep our meager subsidies. (Well, meager compared to what Boulez was receiving.)}{Murail, Smith, 2000}{11}

A ribadire le analogie e le differenze tra i due movimenti si esprime Dufourt \cite{dufourt1987musica}:

\citazioneestesa{Per la musica seriale, l'unità dell'opera risiede, in un certo senso, in disparte da essa stessa. [...] La composizione seriale riposa dunque su una violenza fondamentale, perché deve ridurre ed intrecciare sistemi concorrenti ed impellenti. [...] La forma seriale si manifesta sempre in forme discontinue [...] Ma a differenza dell'estetica seriale, noi concepiamo l'opera musicale come una totalità sintetica [...] La musica seriale adotta un modo di composizione regionale e polinucleare; la musica spettrale adotta il punto di vista della totalità e della continuità operatoria. La prima occulta la sua logica intrinseca e conferisce allo svolgimento dell'opera un'unità latente; la seconda esteriorizza il suo ordine costitutivo e rende manifesta la sua unità. La musica seriale tende a privilegiare l'intuizione del discontinuo e concepisce la musica come un'embricatura di spazi strutturali; la musica spettrale è sostenuta dall'intuizione di una continuità dinamica e pensa la musica come un reticolato d'interazioni. [...] La prima riduce i suoi conflitti con risoluzioni parziali; la seconda con regolazioni.}{Dufourt, 1987}{67-69}

Il gruppo de L'Itinéraire trova nell'analisi spettrale una chiave per indagare i
rapporti armonici e la continuità del suono nella sua natura instabile.
La visione dell’evento sonoro “nota” considerata come entità astratta slegata dalla dimensione fisica e percettiva di un suono, è ciò che nei decenni successivi, sancisce il punto di rottura con la visione della scuola di Darmstadt.
Secondo Murail \cite{doi:10.1080/07494460500154806} un compositore non lavora con note e simboli, ma con suono e tempo. Le rappresentazioni sonore, come le note, sono simboli che indicano solo parzialmente il gesto del musicista e il risultato desiderato. La musica elettronica ha già superato questi limiti categoriali, ma spesso manca di una formalizzazione o di una struttura. Per organizzare questi spazi sonori infiniti, è necessario concentrarsi sulle relazioni tra gli elementi, piuttosto che su riferimenti esterni.



\subsection{Timbro e armonia nella musica spettrale}
Uno degli strumenti centrali in questa esplorazione è la serie naturale degli armonici, che offre una scala naturale con cui comprendere le relazioni tra i suoni. 
Murail esplora l'idea di una congruenza tra l'orizzontale e il verticale nella musica, suggerendo che un certo spettro può essere sfruttato sia in termini armonici che melodici, creando situazioni intermedie in una dimensione "frattale" dove la percezione può oscillare tra diverse letture o abbandonarsi all'ambiguità \cite{doi:10.1080/07494460500154814}. Viene poi introdotto il concetto di funzione e algoritmo, con l'idea che gli spettri armonici e le modulazioni possono seguire modelli matematici relativamente semplici. Per confrontare spettri diversi, è necessaria una gerarchia che classifichi le relazioni frequenziali, da armoniche a inarmonie, e che consideri la specificità di ogni tipo o insieme di relazione di frequenze.


\subsection{Walter Branchi}
Ricapitolato questo percorso storico torniamo alla parentesi ancora aperta riguardo il pensiero di Branchi. 
La musica sistemica (in contrapposizione a quella tematica), come descritta da Branchi, si sviluppa per differenziazione delle componenti implicite di un sistema, piuttosto che seguire un percorso lineare da un inizio a una fine. Non è narrativa e non si sviluppa attraverso unità ordinate nel tempo, ma piuttosto si manifesta in un dominio spaziale in cui tutte le possibilità del sistema sono presenti simultaneamente. In questa prospettiva, la musica sistemica non ha attese o direzionalità: non "racconta" ma "manifesta". Il processo compositivo sistemico si basa su due fasi: la definizione del sistema e la selezione delle sue epifanie, cioè delle manifestazioni udibili che non seguono una logica narrativa o giustappositiva \cite{pizzaleo_branchi_2022}. 

Un sistema sonoro, nella sua dimensione tecnica e matematica, si forma attorno a diversi principi di intonazione, come descritto da Branchi nel suo scritto inedito \textit{Comporre nel suono. Pensare in rapporti.} Prima di addentrarci nei dettagli, è essenziale stabilire quale sistema di intonazione considerare. In \textit{I numeri della musica}, Branchi ne presenta vari, dai più tradizionali come il pitagorico, il naturale e il mesotonico, ai temperamenti equabili, fino ai sistemi più innovativi del Novecento, come quelli di Partch e Alain Daniélou (1907–1994). Quest'ultimo è particolarmente rilevante per Branchi \cite{pizzaleo_branchi_2022}.

Il sistema di Daniélou permette di calcolare, all'interno di un'ottava, 54 valori di frequenza che generano 53 intervalli ineguali. Questi rapporti possono essere trasposti attraverso la moltiplicazione per potenze di 2, consentendo così una flessibilità di applicazione sonora. Da un punto di vista tecnico, il sistema si basa su sette rapporti fondamentali, tra cui quelli della terza minore e della sesta maggiore, e genera ulteriori rapporti attraverso sovrapposizioni. Questi rapporti possono essere espressi in una formula matematica che integra variabili per ottenere risultati sonori distintivi.

La selezione delle frequenze è guidata dall'idea di "insieme," che è un aggregato di sinusoidi caratterizzato da tre proprietà principali: tessitura, categoria e livello di tensione. La tessitura si riferisce alla densità dell'insieme, ovvero come le sue componenti si distribuiscono nello spazio delle frequenze. Branchi fornisce esempi di insiemi con diverse densità, dimostrando che la distribuzione può variare notevolmente anche con lo stesso numero di componenti \cite{pizzaleo_branchi_2022}.

La categoria degli intervalli è la seconda proprietà, che permette di identificare come gli intervalli possono concatenarsi attraverso moltiplicazioni. Le quinte del sistema pitagorico, per esempio, possono essere rappresentate attraverso una formula che indica il numero di quinte sovrapposte alla frequenza di riferimento. Questa categorizzazione conferisce all'insieme un "colore" acustico distintivo.

Infine, il livello di tensione tra i rapporti si riferisce alla dinamica che esiste tra intervalli complessi e più semplici. Gli intervalli più semplici tendono ad attrarre quelli più complessi, creando un equilibrio sonoro e una risoluzione che definisce ulteriormente l'insieme.

In sintesi, un sistema sonoro, secondo Branchi, si sviluppa attraverso un complesso intreccio di relazioni matematiche e scelte artistiche, dove ogni elemento interagisce con gli altri in modo che va oltre la mera somma delle sue parti, creando i presupposti per una ricca e sfumata esperienza musicale.

Pizzaleo spiega che Branchi identifica tre modalità principali di generazione di sistemi di frequenze, ognuna caratterizzata da un crescente numero di gruppi di rapporti. La prima modalità prevede l'uso degli stessi intervalli dell'insieme originale come intervalli di proiezione.
La seconda modalità utilizza due distinti gruppi di rapporti: il primo genera le configurazioni, mentre il secondo fornisce i rapporti di proiezione.
Infine, la terza modalità prevede l'applicazione di rapporti di proiezione a gruppi diversi di frequenze, consentendo configurazioni strutturalmente diverse.
Una volta definito il campo delle frequenze, è fondamentale considerare la variabilità delle ampiezze e delle frequenze stesse. Branchi introduce il concetto di "imprecisione programmata", una deviazione casuale dei valori di frequenza entro limiti stabiliti. Questa imprecisione è programmata in modo da non compromettere il sistema di rapporti predeterminato, garantendo così un costante movimento interno nella trama sonora \cite{pizzaleo_branchi_2022}.

L'approccio di Branchi si pone in netto contrasto con quello della musica seriale. La contrapposizione tra metodo seriale e composizione sistemica presenta aspetti controversi, in quanto elementi di sistemicità possono essere rintracciati anche nella musica seriale. La musica seriale si fonda su un'astrazione, la serie, che, nella sua forma più radicale, il serialismo integrale, struttura ogni aspetto parametricamente della musica, come dinamiche, modalità d'attacco e altezze. Questi parametri si intrecciano in una complessa operazione di stratificazione e tessitura, producendo un evento sonoro che è il risultato combinatorio di una serie di elementi intelligibili.

Ho voluto soffermarmi su Branchi perché lo ritengo una tappa fondamentale nel percorso storico in quanto contemporaneo dei compositori Spettrali e per alcuni aspetti significativamente vicino a questi, per altri enormemente distante. 

Nell'aver osservato alcune delle esperienze in analogia o in contrapposizione al trattamento delle ampiezze nella storia della musica, addentriamoci nella musica spettrale.


