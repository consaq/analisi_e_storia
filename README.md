# Analisi & Storia 

## Lezione 01-08-2024
La scelta è stata orientata sull'analisi del lavoro di Tristan Murail [***Desintegrations***](https://gitlab.com/consaq/analisi_e_storia/-/blob/main/Partiture/Murail-Tristan/Murail_Tristan-Desintegrations.pdf?ref_type=heads) per i seguenti motivi:
- Orchestrazione di parte del nastro nell'orchestra e conseguente coerenza del materiale compositivo.
- Rapporto tra scrittura statistica e deterministica.
- Elementi compositivi che prendono spunto da processamento di segnale elettronico, riportati nella scrittura strumentale.
- Esecuzione con *click*.
- Lavoro più recente e più elaborato se confrontato ai brani di Grisey del decennio precedente (1970) considerati manifesti del movimento spettralista.
Propositi:
- Comprensione a livello estesico della macroforma nei suoi 11 *moments* con attenta visione della [partitura](https://gitlab.com/consaq/analisi_e_storia/-/blob/main/Partiture/Murail-Tristan/Murail_Tristan-Desintegrations.pdf?ref_type=heads).
- Lettura manifesto spettralista Dufourt.
- Lettura scritti di Murail e [bibliografia](https://gitlab.com/consaq/analisi_e_storia/-/tree/main/letture%20spettralismo%20e%20murail?ref_type=heads).
_____________________________
